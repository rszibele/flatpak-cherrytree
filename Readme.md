# Unofficial Flatpak for CherryTree

CherryTree is now officially available on Flathub:
https://flathub.org/apps/details/com.giuspen.cherrytree

Please use that instead.